local wezterm = require 'wezterm'
local config = {}

config.font = wezterm.font('DejaVu Sans Mono', { weight = 'Bold'})
-- config.color_scheme = 'Belafonte Night (Gogh)'

config.window_background_image = '/home/danrobi/Good-Pictures/desktop-background-girl.darked.png'

-- config.selection_fg = none

config.enable_scroll_bar = false

config.cursor_blink_rate = 0

config.default_cursor_style = SteadyBlock

config.hide_tab_bar_if_only_one_tab = true

config.enable_tab_bar = false

config.hide_mouse_cursor_when_typing = true


return config