;; https://github.com/aartaka/nyxt-config black theme
;; nyxt with emacs: https://ag91.github.io/blog/2021/06/22/back-to-emacs-while-in-nyxt-how-to-edit-the-web-in-a-lispy-editor/
;; https://github.com/jmercouris/configuration/blob/master/.config/nyxt/init.lisp#L9
;; Base configuration for nyxt browser with opinionated theme(s) and organization: https://github.com/ericdrgn/drgn.nyxt
;; https://github.com/aartaka/nx-dark-reader

(in-package #:nyxt-user)

;;; ~/.config/nyxt/init.lisp
(load-after-system :nx-dark-reader (nyxt-init-file "dark-reader.lisp"))


;;; ~/.config/nyxt/dark-reader.lisp
;; (define-configuration nx-dark-reader:dark-reader-mode
;;   ;; Note the nxdr: short package prefix. It's for your convenience :)
;;   ((nxdr:brightness 80)
;;    (nxdr:contrast 60)
;;    (nxdr:text-color "white")))
;; Make sure that nx-dark-reader is cloned in the right location,
;; otherwise this will fail with... undesireable consequences :)
(define-configuration web-buffer
    ((default-modes `(nx-dark-reader:dark-reader-mode ,@%slot-default%))))

;; (asdf:load-system :nx-dark-reader)



;; (asdf:load-system :nx-mapper ;; (nyxt-init-file "/home/danrobi/.local/share/nyxt/nx-mapper/src/mapper.lisp"))

;; (define-configuration buffer
;;   ((default-modes
;;     (append
;;      ;; Either of them or both, depending on what functionality you want
;;      '(nx-mapper/stylor-mode:stylor-mode
;;        nx-mapper/stylor-mode:rural-mode)
;;      %slot-default%)))



(asdf:load-system :nx-fruit) ;; https://github.com/atlas-engineer/nx-fruit

(define-configuration browser
    ((session-restore-prompt :always-restore)))

(define-configuration window
    ((message-buffer-style
      (str:concat
       %slot-default%
       (cl-css:css
	'((body
           :background-color "black"
           :color "white")))))))

(define-configuration prompt-buffer
    ((style (str:concat
             %slot-default%
             (cl-css:css
              '((body
		 :background-color "black"
		 :color "white")
		("#prompt-area"
		 :background-color "purple")
		("#input"
		 :background-color "white")
		(".source-name"
		 :color "white"
		 :background-color "gray")
		(".source-content"
		 :background-color "black")
		(".source-content th"
		 :border "1px solid purple"
		 :background-color "white")
		("#selection"
		 :background-color "gray"
		 :color "purple")
		(.marked :background-color "gray"
                 :font-weight "bold"
                 :color "white")
		(.selected :background-color "black"
                 :color "white")))))))


(define-configuration status-buffer
    ((style (str:concat
	     %slot-default%
	     (cl-css:css
	      '(("#container"
		 ;; Columns: controls, url
		 :grid-template-columns "0% 40% 0% 60%")
		("#controls"
		 :border-top "1px solid white")
		("#url"
		 :background-color "purple"
		 :color "white"
		 :border-top "1px solid purple")
		("#modes"
		 :background-color "purple"
		 :border-top "1px solid purple")
		("#tabs"
		 :background-color "black"
		 :color "black"
		 :border-top "1px solid white")))))))

(define-configuration buffer
    ;;If you want to reuse the default map,
    ;;you can use %slot-default instead of (make-keymap ...)
    ((override-map (define-key %slot-default%
                       "S-x" 'execute-command
		       "M-d" 'delete-buffer
		       "M-v" 'nyxt/visual-mode:visual-mode
		       "C-s" 'nyxt/web-mode:search-buffer
                       "C-w" 'delete-current-buffer))))

(define-configuration (buffer web-buffer)
    ((default-modes (append %slot-default%
			    ;;                         '(dark-mode)
			    '(blocker-mode)
                            '(noscript-mode)
			    ;;                         '(noimage-mode)
                            '(reduce-tracking-mode)
                            '(nosound-mode)
         		    '(emacs-mode)
			    '(auto-mode)
;;			    '(dark-reader-mode)
			    ;; '(stylor-mode)
			    ;; '(rural-mode)
                            '(nowebgl-mode)))))

(defvar *my-search-engines*
  (list
   '("python3" "https://docs.python.org/3/search.html?q=~a" "https://docs.python.org/3")
   '("ddg" "https://lite.duckduckgo.com/lite/?q=%s" "https://lite.duckduckgo.com"))
  "List of search engines.")

(define-configuration buffer
    ((search-engines (append (mapcar (lambda (engine) (apply 'make-search-engine engine))
                                     *my-search-engines*)
                             %slot-default%))))
