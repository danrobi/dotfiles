;;; goodie-elfeed.el --- Goodie Elfeed

;;; License: GPLv3+

;;; Code:

(provide 'goodie-elfeed)

(require 'elfeed) 

(defgroup goodie-elfeed nil
  "Customisation group for `goodie-elfeed'."
  :group 'comm)

;;;###autoload
(defun goodie-elfeed/setup ()
  "Setup Elfeed with extras:

* Split pane view via popwin."
  (interactive)
  (setq elfeed-search-print-entry-function #'goodie-elfeed/entry-line-draw))

(defcustom goodie-elfeed/feed-source-column-width 30
  "Width of the feed source column."
  :group 'goodie-elfeed
  :type 'integer)

(defcustom goodie-elfeed/tag-column-width 20
  "Width of the tags column."
  :group 'goodie-elfeed
  :type 'integer)

(defcustom goodie-elfeed/wide-threshold 2.0
  "Minimum width of the window (percent of the frame) to start using the wide layout from."
  :group 'goodie-elfeed
  :type 'float)

(defun -pad-string-to (str width)
  "Pad `STR' to `WIDTH' characters."
  (format (format "%s%%%ds" str width) ""))

(defun -elfeed/feed-stats ()
  "Collect some Elfeed feed statistics.

Returns a list: the unread count, entry count, and feed count."
  (if (and elfeed-search-filter-active elfeed-search-filter-overflowing)
      (list 0 0 0)
    (cl-loop with feeds = (make-hash-table :test 'equal)
             for entry in elfeed-search-entries
             for feed = (elfeed-entry-feed entry)
             for url = (elfeed-feed-url feed)
             count entry into entry-count
             count (elfeed-tagged-p 'unread entry) into unread-count
             do (puthash url t feeds)
             finally
             (cl-return
              (list unread-count entry-count (hash-table-count feeds))))))

(defun elfeed/queue-stats ()
  "Collect some stats about the queue.

Returns a list consisting of the feed count, the remaining feeds,
and the length of the active queue."
  (list (hash-table-count elfeed-db-feeds)
        (length url-queue)
        (cl-count-if #'url-queue-buffer url-queue)))

(defun goodie-elfeed/entry-line-draw (entry)
  "Print ENTRY to the buffer."
  (let* ((title (or (elfeed-meta entry :title) (elfeed-entry-title entry) ""))
         (title-faces (elfeed-search--faces (elfeed-entry-tags entry)))
         (feed (elfeed-entry-feed entry))
         (feed-title
          (when feed
            (or (elfeed-meta feed :title) (elfeed-feed-title feed))))
         (tags (mapcar #'symbol-name (elfeed-entry-tags entry)))
         (tags-str (concat "[" (mapconcat 'identity tags ",") "]"))
         (title-width (- (window-width) goodie-elfeed/feed-source-column-width
                         goodie-elfeed/tag-column-width 4))
         (title-column (elfeed-format-column
                        title (elfeed-clamp
                               elfeed-search-title-min-width
                               title-width
                               title-width)
                        :left))
         (tag-column (elfeed-format-column
                      tags-str (elfeed-clamp (length tags-str)
                                             goodie-elfeed/tag-column-width
                                             goodie-elfeed/tag-column-width)
                      :left))
         (feed-column (elfeed-format-column
                       feed-title (elfeed-clamp goodie-elfeed/feed-source-column-width
                                                goodie-elfeed/feed-source-column-width
                                                goodie-elfeed/feed-source-column-width)
                       :left)))

    (if (>= (window-width) (* (frame-width) goodie-elfeed/wide-threshold))
        (progn
          (insert (propertize feed-column 'face 'elfeed-search-feed-face) " ")
          (insert (propertize tag-column 'face 'elfeed-search-tag-face) " ")
          (insert (propertize title 'face title-faces 'kbd-help title)))
      (insert (propertize title 'face title-faces 'kbd-help title)))))

;;; goodie-elfeed.el ends here
