# dotfiles stuff

# Display Cpu/Mem/WiFi in the Emacs Modeline align-to right.

[![best-mode-line-text-to-right](screenshots/new-modeline-minibar-cpu.png)](https://codeberg.org/danrobi/dotfiles/src/branch/main/screenshots/new-modeline-minibar-cpu.png)

Code [here](https://codeberg.org/danrobi/dotfiles/src/branch/main/emacs/mode-line-text-align-right)

# Vieb-Browser Emacs Theme

[![vieb-emacs-theme](screenshots/vieb-screenshot-help.png)](https://codeberg.org/danrobi/dotfiles/src/branch/main/screenshots/vieb-screenshot-help.png)

Code [here](https://codeberg.org/danrobi/dotfiles/src/branch/main/vieb/vieb-emacs-theme.css)