#!/bin/bash

iwconfig 2>/dev/null | grep -i --color=never 'Link Quality' | awk '{print  100 - $1 "%"}'

## Done
