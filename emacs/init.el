;;; package --- Summary -*- lexical-binding: t -*-

;;; Commentary:
;;; Code:

;;-----------------------------------------------------------------------------------------------------
;; require package stuff

;; (eval-when-compile
;;   (require 'use-package))
;; 
;; (setopt package-archives '(("melpa" . "https://melpa.org/packages/")
;; 			 ("elpa" . "https://elpa.gnu.org/packages/")
;; 			 ("nongnu" . "https://elpa.nongnu.org/nongnu/")))
;; (package-initialize)
;; (package-refresh-contents)
;; (unless (package-installed-p 'use-package)
;;   (package-install 'use-package))
;; 
;; (use-package auto-package-update
;;   :ensure t)
;; 
;; (auto-package-update-maybe) ;; update installed packages at startup if there is an update pending
;; 
;; (setq use-package-always-ensure t)

;; With `use-package-always-ensure t' you can specify :ensure nil for the
;; packages you don’t want to install automatically.
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; add-to-list stuff

;; Disable the async-shell-command popup buffer.

(add-to-list 'display-buffer-alist
	     (cons "\\*Async Shell Command\\*.*" (cons #'display-buffer-no-window nil)))

;;-----
;;

(add-to-list 'exec-path "~/.local/bin/")

(add-to-list 'auto-mode-alist '("\\.\\(org\\)\\'" . org-mode))

;; https://github.com/konrad1977/flycheck-overlay
;;(add-to-list 'load-path "~/.emacs.d/flycheck-overlay/")
;; (require 'flycheck-overlay)


;; Makes the cursor stay vertically centered while scrolling
;; https://github.com/andre-r/centered-cursor-mode.el
(add-to-list 'load-path "/home/danrobi/.emacs.d/centered-cursor-mode")
(require 'centered-cursor-mode)
(global-centered-cursor-mode +1)

(add-to-list 'load-path "/home/danrobi/.emacs.d/my-apps/")
(require 'my-apps)

(add-to-list 'load-path "~/.emacs.d/twitch-stream/")
(require 'twitch-stream)

(require 'bind-key)

;;
;;-----------------------------------------------------------------------------------------------------
;; mode stuff

;;-----
;; not needed in terminal

;;(scroll-bar-mode 0)

;;-----
;;

(display-battery-mode 1)
(display-time-mode 0)
(tool-bar-mode 0)
(menu-bar-mode 0)
(blink-cursor-mode 0)
(electric-pair-mode 1)

;;
;;-----
;; since v30. Completion Preview mode update the suggestion as you type.
;; Use "C-M-i" to expend a completion list candidats.

(global-completion-preview-mode 1)

;;
;;-----
;; Hightlight the whole line mode.

(global-hl-line-mode 0)

;;-----
;; reverts any buffer associated with a file when the file changes on disk

(global-auto-revert-mode 1)

;;---------------------
;; Mode that records window configuration. Changes can be "undone" and "redo".

(winner-mode 1)

;;---------------------
;; When enabled, the echo area displays information
;; about a function or variable in the text where point is.

(global-eldoc-mode 1)

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; Toggle use of Ibuffer’s auto-update facility (Ibuffer Auto mode).

(add-hook 'ibuffer-mode-hook 'ibuffer-auto-mode)

;;
;;-----------------------------------------------------------------------------------------------------
;; Set wrap limit to 130 characters in Shell-mode and Eshell-mode

(add-hook 'shell-mode-hook
          (lambda ()
            (setopt fill-column 130)))

(add-hook 'eshell-mode-hook
          (lambda ()
            (setopt fill-column 130)))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; `load-theme' stuff


(add-to-list 'custom-theme-load-path "/home/danrobi/.emacs.d/")

(add-hook 'after-init-hook
	  (lambda ()
	    (load-theme 'my-emacs-in-terminal-theme)))

(setq custom-safe-themes t)

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; Hightlight rotate face in specific modes.

(add-hook 'text-mode-hook
	  (function (lambda ()
		      (add-hook 'write-file-functions 'highlight-changes-rotate-faces)
		      (highlight-changes-mode t))))

(add-hook 'emacs-lisp-mode-hook
	  (function (lambda ()
		      (add-hook 'write-file-functions 'highlight-changes-rotate-faces)
		      (highlight-changes-mode t))))


(add-hook 'org-mode-hook
	  (function (lambda ()
		      (add-hook 'write-file-functions 'highlight-changes-rotate-faces)
		      (highlight-changes-mode t))))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; ibuffer stuff

(setopt ibuffer-formats '((mark modified " "
				(name 60 18 :left :elide)
				" "
				(size 9 -1 :right)
				" "
				(mode 16 16 :left :elide)
				" " filename-and-process)

			  (mark modified " "
				(name 70 18 :left :elide)
				" "
				" " filename)

			  (mark modified " "
				(name 50))))

(setopt ibuffer-saved-filters
	(quote (("dired"
		 (derived-mode . dired-mode)))))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; setq stuff

;; (setq hl-line-mode nil) ;; buffer local only
(setq visible-cursor nil) ;; Disable cursor blinking in terminal mode
(setq icomplete-in-buffer t) ;; If non-nil, use Icomplete when completing in buffers other than minibuffer.
(setopt dictionary-server "dict.org")
(setopt completion-styles '(basic partial-completion substring initials flex))
(setq create-lockfiles t) ;; lockfile creation between sessions. Prevent editing same file between diff session.
(setq read-file-name-completion-ignore-case t)
(setq auto-package-update-prompt-before-update t) ;; show a manual prompt before automatic updates
(setq auto-package-update-delete-old-versions t) ;; delete residual old version directory when updating
(setopt completions-format 'one-column) ;;
(setq global-auto-revert-non-file-buffers t)
(setopt which-func-update-delay 1.0)
(setq eww-header-line-format nil) ;; turn  off header-line in eww buffers
(setq debug-on-error t)
(setq debug-on-quit nil)
(setopt dired-recursive-copies 'always) ;; If nil, never copy recursively
(setopt dired-recursive-deletes 'always) ;; ‘always’ means to delete non-empty directories recursively
(setopt custom-file null-device) ;; do not create custom-file
(setq truncate-partial-width-windows nil) ;; nil means to respect the value of ‘truncate-lines’
(setopt display-buffer-alist '((".*" display-buffer-full-frame)))
(setopt browse-url-browser-function 'eww-browse-url)
(setopt org-link-elisp-confirm-function 'y-or-n-p)
(setq use-short-answers t)
(setq enable-recursive-minibuffers t) ;; Allow another minibuffer command while in the minibuffer.
(setq shr-inhibit-images t) ;; eww-toggle-images ;; disable displaying images in eww and elfeed. Possibly org-mode as well?
(setq dired-auto-revert-buffer t) ;;  Dired should refresh the listing on each revisit.
(setq inhibit-startup-message t) ;; enable/disable gnu splash screen
(setq completion-ignore-case t) ;; Non-nil means don’t consider case significant in completion
(setq case-fold-search t) ;; Non-nil searches and matches should ignore case
(setopt url-privacy-level 'paranoid) ;; How private you want your requests to be
(setopt network-security-level 'medium) ;; Default.  Suitable for most circumstances
(setq auto-revert-verbose nil) ;; When nil, Auto-Revert Mode does not generate any messages
(setq ibuffer-use-header-line nil) ;; nil remove header line
(setopt Buffer-menu-name-width 50) ;; list-buffer / buffer menu name width
(setopt cursor-type 'box)
(setopt ibuffer-default-sorting-mode 'recency)

;; v30
(setopt completions-sort 'historical) ;; rearranges the order according to the order of the candidates in the minibuffer history
(setopt Buffer-menu-group-by '(Buffer-menu-group-by-mode))

;;
;;-----------------------------------------------------------------------------------------------------
;; nil, t, or 'yes.  'yes means the same as t except that mixed
;; case in the search string is ignored

(setopt isearch-case-fold-search 'yes)

;;
;;-----------------------------------------------------------------------------------------------------
;; Remove duplicates from the kill ring to reduce clutter

(setq kill-do-not-save-duplicates t)

;;
;;-----------------------------------------------------------------------------------------------------
;; Ensures that empty lines within the commented region are also commented out.
;; This prevents unintended visual gaps and maintains a consistent appearance,
;; ensuring that comments apply uniformly to all lines, including those that are
;; otherwise empty

(setq comment-empty-lines t)

;;
;;-----------------------------------------------------------------------------------------------------
;; Eliminate delay before highlighting search matches

(setopt lazy-highlight-initial-delay 0.25)

;;
;;-----------------------------------------------------------------------------------------------------
;; Controls the operation of the TAB key.
;; If t, hitting TAB always just indents the current line.
;; If nil, hitting TAB indents the current line if point is at the left margin
;; or in the line’s indentation, otherwise it inserts a "real" TAB character.
;; If ‘complete’, TAB first tries to indent the current line, and if the line
;; was already indented, then try to complete the thing at point.

(setopt tab-always-indent 'complete)

;;
;;-----------------------------------------------------------------------------------------------------
;; Move the mouse to the upper-right corner on any keypress
;; Does not work in terminal. Use `mouse_hide_wait -1' in the kitty.conf file instead.

;; (setq mouse-avoidance-mode 'banish)

;;
;;-----------------------------------------------------------------------------------------------------
;; Switches Passed To ‘ls’ For Dired. Sort Directories First
;; https://truongtx.me/2013/04/01/emacs-sort-directories-first-in-dired

(setq-default dired-listing-switches "--group-directories-first -alh")

;;
;;-----------------------------------------------------------------------------------------------------
;; shows empty lines at the end of the buffer

(setq-default indicate-empty-lines t)

;;
;;-----------------------------------------------------------------------------------------------------
;; Continue wrapped lines at whitespace rather than breaking in the
;; middle of a word

;; (setq-default word-wrap t)

;;
;;-----------------------------------------------------------------------------------------------------
;;

(setq-default truncate-lines t)

;;
;;-----------------------------------------------------------------------------------------------------
;; explicitly set mode-line-end-spaces to an empty string

(setq-default mode-line-end-spaces "")

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; Truncation stuff.
;; Ensure a display table exists.

(setq-default standard-display-table
              (or standard-display-table
                  (make-display-table)))


;;---------------------
;; Truncation stuff.
;; Replace the truncation marker with a space.
;; Don't show the $ at end of truncated line.

(set-display-table-slot standard-display-table 'truncation ?\ )

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; Set Margin/Fringe Left/Right.

(setq-default left-margin-width 5)
(setq-default right-margin-width 5)
(setq-default right-fringe-width 0)
(setq-default left-fringe-width 0)

;;
;;-----------------------------------------------------------------------------------------------------
;; Set up / down margin window. Does not wotk in terminal

;; (set-frame-parameter nil 'internal-border-width 20)

;;
;;-----------------------------------------------------------------------------------------------------
;; Setting `display-time-default-load-average' to nil makes Emacs omit the load
;; average information from the mode line.
;; Not needed with `minibar-mode'.

(setq display-time-default-load-average nil)

;;
;;-----------------------------------------------------------------------------------------------------
;; trailing spaces can be removed with: M-x delete-trailing-whitespace RET
;; Non-nil means highlight trailing whitespace

(setq-default show-trailing-whitespace nil)

;;
;;-----------------------------------------------------------------------------------------------------
;;

(setq-default safe-local-variable-directories: '("~/.local/bin/"))

;;
;;-----------------------------------------------------------------------------------------------------
;; Must be set to a symbol.  Acceptable values are:
;; - ‘window’: align to extreme right of window, regardless of margins
;;   or fringes
;; - ‘right-fringe’: align to right-fringe
;; - ‘right-margin’: align to right-margin

(set-default mode-line-right-align-edge 'right-fringe)

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; Open specific Buffers at startup  

(setq-default initial-buffer-choice
      (lambda ()
        ;; Open *shell*
        (shell)
	(list-processes)
	(proced)
        ;; Open elfeed-search if elfeed is installed
        (when (fboundp 'elfeed)
          (elfeed))
        ;; Open ~/.emacs.d/init.el
        (find-file "~/.emacs.d/init.el")
        ;; Open ~/.emacs.d directory
        (dired "~/.emacs.d")
        ;; Return to *scratch* buffer
        (get-buffer "*scratch*")))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; https://www.jamescherti.com/emacs-symbol-highlighting-built-in-functions/
;; Toggling symbol highlighting with unique colors for each symbol

(require 'hi-lock)  ; Built-in Emacs package

(defun simple-toggle-highlight-symbol-at-point ()
  "Toggle highlighting for the symbol at point."
  (interactive)
  (when-let* ((regexp (find-tag-default-as-symbol-regexp)))
    (if (member regexp (hi-lock--regexps-at-point))
        ;; Unhighlight symbol at point
        (hi-lock-unface-buffer regexp)
      ;; Highlight symbol at point
      (hi-lock-face-symbol-at-point))))

;; You can configure a key binding, such as C-c h, with the following:
;; (global-set-key (kbd "C-c h") #'simple-toggle-highlight-symbol-at-point)Code language: Lisp (lisp)
;; You can also remove a symbol highlight from the entire buffer by selecting it from the list and removing it using:
;; M-x hi-lock-unface-buffer
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;;("C-z I")
;; iwb stuff

(defun iwb ()
  "Indent Whole Buffer."
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; ("C-z y")
;; Copy Whole Line.

(defun copy-line (arg)
  "Copy lines as ARG (as many as prefix argument) in the kill ring."
  (interactive "p")
  (kill-ring-save (line-beginning-position)
		  (line-beginning-position (+ 1 arg)))
  (message "%d line%s copied" arg (if (= 1 arg) "" "s")))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; ("C-z C-e") ;; Replaced with: emacs-lisp-byte-compile-and-load
;; Reload the init file stuff
;; https://git.sr.ht/~x4d6165/.emacs.d/tree/master/item/settings.org

(defun reload-init-file ()
  "Reload the Emacs init.el file."
  (interactive)
  (load-file "~/.emacs.d/init.el"))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; Hook to auto Rename *Occur* buffer to *Occur: original-buffer-name<regexp>
;; https://old.reddit.com/r/emacs/comments/y0rq1d/help_occur_buffer_include_both_the_buffer_name/

(defun my/occur-rename-buffer ()
  "Occur Rename Buffer."
  (interactive)
  (rename-buffer
   (format "%s<%s>"
           (let (buffer-list-update-hook)
             (occur-rename-buffer))
           (regexp-quote (car occur-revert-arguments)))))

(add-hook 'occur-hook 'my/occur-rename-buffer)

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; press "V" dired open file with feh image stuff
;; dired open file externally with feh

(defun open-dired-with-feh (file)
  "Open FILE With feh."
  (start-file-process "feh-id" nil "feh" "-." "-x" file))

;; press "V" dired open file with feh
(defun dired-open-externally-with-feh (file)
  "Open marked FILE or current file with feh."
  (interactive "P")
  (dired-map-over-marks
   (open-dired-with-feh (dired-get-filename))
   file))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; Function used with eww

(defun eww-download-webpage-with-wget (url)
  "Eww Download WebPage URL."
  (interactive "sPaste WebPage URL ")
  (start-process "wget-buffer-process-id" "wget-buffer-name" "wget"
		 "-nd" "--directory-prefix=/home/danrobi/.emacs.d/emacs-stuff/blog-org.file/directory-html-file/"
		 "--html-extension" "--convert-links" "--no-cookies" "--no-http-keep-alive" url))

;;
;;-----------------------------------------------------------------------------------------------------
;; Function used with elfeed and eww.
;; "V" Feh view URL image from:
;; elfeed-show-mode and eww-mode

(defun feh-view-url-image (url)
  "Feh view URL image."
  (interactive "sPaste Image URL ")
  (start-process "feh-id" nil "feh"
		 "-."
		 "-x" url))

;;
;;---------------------
;; Function used with elfeed and eww.
;; press "e" to paste youtube url from:
;; elfeed and eww.
;; Globally with "C-z m"

(defun yt-dlp-download-url (url)
  "YT-DLP Download URL."
  (interactive "sPaste YouTube URL ")
  (start-process "yt-dlp-buffer-process-id" "yt-dlp-buffer-name" "yt-dlp"
		 "-f18"
		 "-P~/Videos-Youtube-Downloads" url))

;;
;;---------------------------------------------------------------
;;
;;
;;---------------------Eww---------------------
;;

(use-package eww
  :ensure nil
  :config
  (setq eww-download-directory "~/Videos-Youtube-Downloads"
	eww-auto-rename-buffer 'title ;; choice: title, url or nil. When nil, do not rename the buffer
	eww-buffer-name-length 30 ;; buffer-name in mode-line
	eww-url-transformers '(eww-remove-tracking))
  :bind (:map eww-mode-map
	      ("y" . eww-copy-page-url)
	      ("D" . eww-download-webpage-with-wget)
	      ("d" . eww-download)
	      ("e" . yt-dlp-download-url)
	      ("V" . feh-view-url-image)))

;; Copy RRS Feed Address within Eww
;; eww-copy-alternate-url
;; Associated RSS feeds. It is bound to 'A' by default
;; If there are multiple alternate links, prompt for one in the minibuffer
;; with completion.  If there are none, return nil.
;;---------------------------------------------------------------
;;
;;
;;---------------------Marginalia---------------------
;; Enrich existing commands with completion annotations.

(use-package marginalia
  :ensure t
  :init
  :config
  (marginalia-mode 1))

;;
;;---------------------------------------------------------------
;;
;;
;;---------------------Aggressive-Completion---------------------
;; Automatically completes for you after a short delay.

(use-package aggressive-completion
  :ensure t
  :config
  (aggressive-completion-mode))


(setopt aggressive-completion-delay 5) ;; aggressive-completion-mode

;;
;;---------------------------------------------------------------
;;
;;
;;---------------------Elfeed---------------------
;; Elfeed Rss Feed Client for Emacs

(use-package elfeed
  :ensure t
  :config
  (load (expand-file-name "~/.elfeed/elfeed.el"))
  (setq-default elfeed-db-directory "~/.elfeed") ;;    Directory where elfeed will store its database
  (setq-default elfeed-search-filter "@1-week-ago +unread =")
  (setopt elfeed-show-truncate-long-urls t ;; When non-nil, use an ellipsis to shorten very long displayed URLs
	  elfeed-use-curl t ;; If non-nil, fetch feeds using curl instead of ‘url-retrieve’
	  elfeed-show-entry-switch 'switch-to-buffer ;; Function used to display the feed entry buffer
	  elfeed-search-date-format '("%Y-%m-%d" 0 :right) ;; the "0" means disable date
	  elfeed-search-trailing-width 0
	  elfeed-search-title-max-width 200
	  elfeed-search-title-min-width 200)
  :bind (:map elfeed-search-mode-map
	      ("e" . yt-dlp-download-url))
  (:map elfeed-show-mode-map
	("e" . yt-dlp-download-url)
	("V" . feh-view-url-image)))

(add-hook 'elfeed-search-mode-hook (lambda () (hl-line-mode -1)))

;;
;;---------------------
;; elfeed stuff

(defun elfeed-mark-all-as-read ()
  "Mark all unread feeds as read"
  (interactive)
  (elfeed-untag 'elfeed-search-entries 'unread)
  (elfeed-search-update :force))

;;
;;---------------------
;;
;;
;;---------------------Dslide-Presentation---------------------
;;

(use-package dslide
  :ensure t)

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------Elpher------------------------------------------------------------------------
;; elpher stuff

(use-package elpher
  :ensure t)

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;--------------------Vundo---------------------------------------------------------------------------------
;; Visualize the undo tree
;; https://github.com/casouri/vundo
;; "C-x u"

(use-package vundo
  :ensure t)

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; `xclip' fix clipboard issue when (emacs -nw).
;; https://elpa.gnu.org/packages/xclip.html

(use-package xclip
  :ensure t
  :config
  (xclip-mode))

;;
;;-----------------------------------------------------------------------------------------------------
;; minibar stuff
;; Display a message temporarily without affecting minibar's group messages.

(defun my-temporary-message (orig-fun &rest args)
  "Display a message temporarily without affecting minibar's group messages.
Messages from `minibar-group-right` and `minibar-group-left` are left untouched.
Other messages are cleared after 4 seconds if they remain unchanged."
  ;; Ensure that the format string (the first arg) is non-nil.
  (let* ((fmt (if (null (car args)) "" (car args)))
         (rest-args (cdr args))
         (msg (apply #'format fmt rest-args)))
    ;; Call the original `message` using our non-nil format string.
    (apply orig-fun (cons fmt rest-args))
    ;; Schedule clearing the message after 4 seconds, if it's non-empty
    ;; and the command isn't one of the minibar-group functions.
    (when (and (not (string-empty-p msg))
               (not (memq this-command '(minibar-group-right minibar-group-left))))
      (run-at-time "4 sec" nil
                   (lambda (message-to-clear)
                     (when (equal (current-message) message-to-clear)
                       (message "")))
                   msg))))

(advice-add 'message :around #'my-temporary-message)

;;
;;-----------------------------------------------------------------------------------------------------
;; minibar stuff

(defun my-elfeed-search-filter ()
  "Show elfeed-search-filter in minibar."
  (interactive)
  (when (eq major-mode 'elfeed-search-mode) (elfeed-search--header)))

;;
;;-----------------------------------------------------------------------------------------------------
;; minibar stuff

(defun my-eww-current-url ()
  "Return the current URL of the eww buffer.
Truncated to 30 characters, for use in minibar."
  (when (eq major-mode 'eww-mode)
    (let ((url (eww-current-url)))
      (when url
        (truncate-string-to-width url 60)))))


(defun my-buffer-file-name ()
  "Return the current file name PATH of the buffer.
Truncated to 50 characters, for use in minibar."
  (when '(eq #'(major-mode (text-mode) (emacs-lisp-mode) (org-mode) (fundamental-mode)))
    (let ((file (buffer-file-name)))
      (when file
        (truncate-string-to-width file 60)))))

;;
;;-----------------------------------------------------------------------------------------------------
;; minibar stuff
;; Memory and Wifi Usage.

(defun mem-usage ()
  "Display MEM Usage."
  (shell-command-to-string "/home/danrobi/.emacs.d/mem_info.sh"))

(defun wifi-usage ()
  "Display WiFi Usage."
  (shell-command-to-string "/home/danrobi/.emacs.d/wifi_info.sh"))

;;
;;------------------------Minibar-----------------------------------------------------------------------------
;; https://codeberg.org/akib/emacs-minibar

(use-package minibar
  :ensure t
  :config
  (setq minibar-group-right '(minibar-module-time minibar-module-cpu mem-usage wifi-usage))
  (setq minibar-group-middle 'nil)
  (setq minibar-group-left '(my-buffer-file-name my-elfeed-search-filter my-eww-current-url))
  (setq minibar-module-cpu-cache-for 5)
  ;;  (setq minibar-module-network-speeds-cache-for 5)
  (setq minibar-update-interval 5)
  (minibar-mode))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; Disable `header-line' is a global function.

(defun disable-header-line ()
  "Completely disable the Emacs header line."
  (interactive)
  (setq-default header-line-format nil)
  (dolist (buffer (buffer-list))
    (with-current-buffer buffer
      (setq header-line-format nil))))

;; Run the function at startup
(add-hook 'elfeed-search-update-hook #'disable-header-line)

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; insert-date-and-time stuff

(defun insert-date-and-time ()
  "Insert a timestamp according to locale's date and time format."
  (interactive)
  (insert (format-time-string "%c" (current-time))))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; ("<f9>")
;; oantolin stuff
;; Open file externally
;; https://old.reddit.com/r/emacs/comments/paqvav/how_to_open_different_types_of_file_with_one/
;; open files with external system default apps

(defun open-file-externally (file) ;; "f9"
  "Open FILE Externally."
  (call-process (pcase system-type
		  (_ "xdg-open"))
		nil 0 nil
		(expand-file-name file)))

;; ("<f9>")
(defun dired-open-externally (arg)
  "Open marked ARG or current file in operating system's default application."
  (interactive "P")
  (dired-map-over-marks
   (open-file-externally (dired-get-filename))
   arg))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; press "e" dired open file with mpv
;; dired open file externally with mpv
;; `nil' replace the name buffer. So no log buffer is created.
;; If u want a log buffer replace `nil' with `"buffer-name"'

(defun open-file-externally-with-mpv (file)
  "Open FILE With MPV."
  (start-file-process "mpv-buffer-process-id" nil "mpv"
		      ;; disable with Spectrwm		      "--geometry=1366x665+0+0"
		      "--volume=60" "--ontop=no" "--border=no" "--keep-open=yes" file))

;; press "e" dired open file with mpv
(defun dired-open-externally-with-mpv (file)
  "Dired Open FILE With MPV."
  (interactive "P")
  (dired-map-over-marks
   (open-file-externally-with-mpv (dired-get-filename))
   file))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; ("C-z P")
;; open PDF files externally with qpdfview ("C-z P")

(defun open-file-externally-with-qpdfview (file)
  "Open FILE Externally With Qpdview."
  (call-process (pcase system-type
		  (_ "qpdfview"))
		nil 0 nil
		(expand-file-name file)))

;; ("C-z P")
(defun dired-open-externally-with-qpdfview (file)
  "Open marked FILE or current file with qpdfview."
  (interactive "P")
  (dired-map-over-marks
   (open-file-externally-with-qpdfview (dired-get-filename))
   file))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; ("C-z F")
;; open tar/zip files externally with file-roller

(defun open-file-externally-with-file-roller (file)
  "Open FILE Externally-With-File-Roller."
  (call-process (pcase system-type
		  (_ "file-roller"))
		nil 0 nil
		(expand-file-name file)))

;; ("C-z F")
(defun dired-open-externally-with-file-roller (arg)
  "Open marked ARG or current file with file roller."
  (interactive "P")
  (dired-map-over-marks
   (open-file-externally-with-file-roller (dired-get-filename))
   arg))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; shell-instead-dired

(defun shell-instead-dired ()
  "Open the Dired current Directory In Shell term."
  (interactive)
  (shell (concat default-directory "-shell")))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; Prevent org-mode spliting windows. Open / Display Org-link in same Buffer / Window
;; https://emacs.stackexchange.com/questions/62720/open-org-link-in-the-same-window

(setq-default org-link-frame-setup
	      '((vm . vm-visit-folder-other-frame)
		(vm-imap . vm-visit-imap-folder-other-frame)
		(gnus . org-gnus-no-new-news)
		(file . find-file)
		(occur . find-file) ;; this fix the issue with 'my/occur-rename-buffer
		(wl . wl-other-frame)))

;;
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;---------------------Presentation-Mode---------------------------------------------------------------
;; Hide the `mode-line' function.
;; To be used in presentation mode, dslide stuff.

(defun presentation-mode-hide-modeline ()
  "Hide modeline in presentation mode.
Set background color black"
  (interactive)
  (setopt mode-line-format t)
  (set-background-color "black")
  (set-foreground-color "white")
  (set-face-attribute 'mode-line nil
		      :weight 'light
		      :background "black"
		      :foreground "black"
		      :box nil)
  ;; this set modeline height
  (set-face-attribute 'mode-line nil :height 1.5))

;;
;;-------------------Dired----------------------------------------------------------------------------------
;;

(use-package dired
  :ensure nil
  :config
  :bind (:map dired-mode-map
	      ("E" . dired-do-eww)
	      ("RET" . dired-display-file)
	      ("I" . dired-kill-subdir)
	      ("e" . dired-open-externally-with-mpv)
	      ("V" . dired-open-externally-with-feh)))

;;
;;-----------------------------------------------------------------------------------------------
;;
;;
;;------------------Org-Button-List-------------------------------------------------------------------------
;; ("C-z O")
;; Org-Button-List

(defun org-button-list ()
  "Open Org-Button-List ."
  (interactive)
  (find-file "~/.emacs.d/emacs-stuff/Org-Button-List.org"))

;; Org-Button-List ends here
;; ("C-z O")
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;---------------------Keybinds--------------------------------------------------------------------------
;; My Keybinds stuff
;; https://www.reddit.com/r/emacs/comments/3ytb6n/a_better_way_to_define_a_new_prefix/

;; Personal Prefix-Command (kbd "C-z") stuff

(define-prefix-command 'z-map)
(global-set-key (kbd "C-z") 'z-map)
(global-set-key (kbd "C-z u") 'elfeed-update)
(global-set-key (kbd "C-z s") 'bookmark-set)
(global-set-key (kbd "C-z j") 'bookmark-jump)
(global-set-key (kbd "C-z l") 'list-bookmarks) ;; Display list of existing bookmarks. Delete bookmark from here
(global-set-key (kbd "C-z C-e") 'emacs-lisp-byte-compile-and-load)
(global-set-key (kbd "C-z O") 'org-button-list)
(global-set-key (kbd "C-z C-z") 'list-matching-lines) ;; occur search find buffer
(global-set-key (kbd "C-z R") 'replace-regexp)
(global-set-key (kbd "C-z y") 'copy-line) ;; copy whole line
;; (global-set-key (kbd "C-z O") 'replace-spaces-with-dashes)
(global-set-key (kbd "C-z m") 'yt-dlp-download-url)
(global-set-key (kbd "C-z d") 'eww-download-webpage-with-wget)
(global-set-key (kbd "C-z i") 'iwb) ;; indent whole buffer
(global-set-key (kbd "C-z f") 'menu-find-file-existing)
;; (global-set-key (kbd "C-z C") 'dired-open-externally-with-musikcube)
(global-set-key (kbd "C-z F") 'dired-open-externally-with-file-roller)
;; (global-set-key (kbd "C-z c") 'dired-open-externally-with-QMP)
(global-set-key (kbd "C-z P") 'dired-open-externally-with-qpdfview)
(global-set-key (kbd "C-z D") 'describe-face) ;; describe face at point
(global-set-key (kbd "C-z w") 'wdired-change-to-wdired-mode) ;; replace

;; Personal Prefix-Command (kbd "C-z") stuff ends here

(global-set-key (kbd "C-x C-c") 'save-buffers-kill-emacs) ;; Offer to save each buffer, then kill emacs
(global-set-key (kbd "M-w") 'kill-ring-save) ;; copy selected region
(global-set-key (kbd "C-y") 'yank) ;; paste
(global-set-key (kbd "C-w") 'kill-region) ;; cut selected region
(global-set-key (kbd "C-k") 'kill-whole-line) ;; Kill/cut the rest of the current line. forward under point. Cut the line
(global-set-key (kbd "M-y") 'yank-pop) ;; reinserted/yank/paste previously-killed/copied text

;; Text Smaller Reduce and Text Bigger Larger
;; Does not work in terminal. Use termnial zoom keybind instead
;; (global-set-key (kbd "C--") 'text-scale-decrease)
;; (global-set-key (kbd "C-=") 'text-scale-increase)

(global-set-key (kbd "C-x C-f") 'find-file)
(global-set-key (kbd "M-x") 'execute-extended-command)
(global-set-key (kbd "C-x d") 'ido-dired)
(global-set-key (kbd "C-x C-x") 'kill-current-buffer)
(global-set-key (kbd "C-x t") 'erc-track-switch-buffer) ;; switch to next erc-buffer new message
(global-set-key (kbd "C-c C-<left>") 'winner-undo) ;; Switch back to an earlier window configuration saved by Winner mode
(global-set-key (kbd "C-c C-<right>") 'winner-redo) ;; Restore a more recent window configuration saved by Winner mode
;; (global-set-key (kbd "<menu>") 'ibuffer) ;; with external mechanical keyboard
(global-set-key (kbd "C-b") 'ibuffer) ;; Global Use
;; (global-set-key (kbd "<print>") 'ibuffer) ;; The "<PrtSc>" key while running the emacs-gtk-gui
;; (global-set-key (kbd "M-[ 5 7 3") 'ibuffer) ;; For "<prtsc>" key while in terminal
;; (global-set-key (kbd "<kp-insert>") 'kill-current-buffer)
;; (global-set-key (kbd "<kp-end>") 'list-matching-lines)
;; (global-set-key (kbd "<kp-down>") 'delete-window) ;; delete current window
(global-set-key (kbd "<M-left>") 'windmove-left)
(global-set-key (kbd "<M-right>") 'windmove-right)
(global-set-key (kbd "<M-up>") 'windmove-up)
(global-set-key (kbd "<M-down>") 'windmove-down)
(global-set-key (kbd "C-x u") 'vundo) ;; displays the undo history as a tree
(global-set-key (kbd "C-M-x") 'eval-defun)
(global-set-key (kbd "C-x e") 'elfeed) ;; open elfeed
(global-set-key (kbd "<f9>") 'dired-open-externally) ;; oantolin stuff
;; (global-set-key (kbd "<kp-add>") 'text-scale-increase)
;; (global-set-key (kbd "<kp-subtract>") 'text-scale-decrease)
(global-set-key (kbd "C-x l") 'count-lines-page) ;; Report number of lines on current page

;; or 'M-TAB'. Perform completion on the text around point
(global-set-key (kbd "C-M-i") 'completion-at-point)

;; keyboard-escape-quit Exit the current "mode". Get out of the minibuffer. Go back to just one window (deleting all but the selected window)
;; (global-set-key (kbd "<escape>") 'keyboard-escape-quit) ;; does not work in terminal
;; (define-key dired-mode-map (kbd "I") 'dired-kill-subdir)
;; (bind-key "E" 'dired-do-eww dired-mode-map)
(global-set-key (kbd "M-p") 'beginning-of-buffer)
(global-set-key (kbd "M-n") 'end-of-buffer)

;; v 30.1
;; New command 'dired-do-open'.
;; This command is bound to 'E' (mnemonics "External").  Also it can be
;; used by clicking "Open" in the context menu; it "opens" the marked or
;; clicked on files according to the OS conventions.  For example, on
;; systems supporting XDG, this runs 'xdg-open' on the files.

;; (unbind-key (kbd "<RET>") dired-do-open)
(unbind-key (kbd "M-<down>") completion-list-mode-map)
(define-key completion-list-mode-map (kbd "M-n") #'minibuffer-next-completion)

;; (bind-key "C-M-<left>" 'previous-buffer)
(global-set-key (kbd "C-M-<left>") 'previous-buffer)

;; (bind-key "C-M-<right>" 'next-buffer)
(global-set-key (kbd "C-M-<right>") 'next-buffer)

(global-set-key [remap org-priority] #'org-insert-structure-template) ;; "C-c C-,"

;; (global-set-key (kbd "C-x <f5>") (lambda() (interactive)(load-file "~/.emacs.d/init.el")))
;; (global-set-key (kbd "M-[ 5 7 3") 'list-buffers)
;; (keymap-global-set "M-[ 5 7 3" 'list-buffers)

;;----------------------
;; org keybind

(with-eval-after-load 'org
  (define-key org-mode-map (kbd "C-c b") 'org-fold-hide-block-toggle)
  (define-key org-mode-map (kbd "C-c d") 'org-fold-hide-drawer-toggle)
  (define-key org-mode-map (kbd "C-c l") 'org-toggle-link-display))

;;
;;---------------------

;; New key binding after 'M-x' or 'M-X': 'M-X'
;; 'M-X' (note upper case X) command, which only displays commands
;; especially relevant to the current buffer.
;;-----------------------------------------------------------------------------------------------------
;;
;;
;;-----------------------------------------------------------------------------------------------------
;; aliases

(defalias 'oit 'org-insert-structure-template)
(defalias 'oeh 'org-html-export-to-html)
(defalias 'dds 'dslide-deck-start)
(defalias 'ttl 'toggle-truncate-lines)
(defalias 'otl 'org-toggle-link-display)
(defalias 'ds 'desktop-save)
(defalias 'dr 'desktop-read)


;;
;;-----------------------------------------------------------------------------------------------------

(provide 'init)
;;; init.el ends here
