#!/bin/bash

free | awk '/Mem:/ {printf "%.0f\n", $3/$2 * 100}' | tr -cd '\40-\176'

## Done
