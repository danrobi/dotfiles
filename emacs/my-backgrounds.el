;;; package --- Summary
;;; Commentary:
;;; Code:
;; -----------------------------------------------------------------------------------------------------


;; Backgrounds
;; (defun snow-background ()
;;   "Snow Theme"
;;   (interactive)
;;   (set-face-background #'mode-line-inactive "snow")
;;   (set-face-foreground #'mode-line-inactive "black")
;;   (set-face-background #'mode-line "black")
;;   (set-face-foreground #'mode-line "gray")
;;   (set-background-color "snow")
;;   (set-foreground-color "black")
;;   (set-face-background 'elfeed-search-title-face "snow")
;;   (set-face-foreground 'elfeed-search-title-face "red")
;;   (set-face-background 'elfeed-search-unread-title-face "snow")
;;   (set-face-foreground 'elfeed-search-unread-title-face "purple")
;;   (set-face-attribute 'region nil :background "gray"))

;; (defun lightsteelblue4-background ()
;;   "LightSteelBlue4 Theme"
;;   (interactive)
;;   (set-face-background #'mode-line-inactive "LightSteelBlue4")
;;   (set-face-foreground #'mode-line-inactive "gray")
;;   (set-face-background #'mode-line "black")
;;   (set-face-foreground #'mode-line "gray")
;;   (set-background-color "LightSteelBlue4")
;;   (set-foreground-color "black")
;;   (set-face-background 'elfeed-search-title-face "snow")
;;   (set-face-foreground 'elfeed-search-title-face "black")
;;   (set-face-background 'elfeed-search-unread-title-face "black")
;;   (set-face-foreground 'elfeed-search-unread-title-face "purple")
;;   (set-face-attribute 'region nil :background "snow"))


;; -----------------------------------------------------------------------------------------------------
;; my-backgrounds.theme thingy

(defun my-backgrounds.theme.el ()
  "Black Theme."
  (interactive)
  (set-face-attribute 'header-line nil
		      :weight 'light
		      :foreground "black"
		      :background "black"
		      :box nil)
  (set-face-attribute 'mode-line-inactive nil
		      :weight 'light
		      :foreground "black"
		      :background "black"
		      :box '(:line-width 20
					 :color "black"
					 :weight 'bold))
  (set-face-attribute 'mode-line nil
		      :weight 'bold
		      :background "black"
		      :foreground "dark violet"
		      :box '(:line-width 20
					 :color "black"
					 :weight 'bold))
  (set-face-attribute 'highlight nil ;; hightlight hovering line
		      :background "gray5"
		      :foreground "seashell1")
  (set-face-attribute 'show-paren-match nil
		      :weight 'light
		      :background "white"
		      :box nil)
  (set-face-attribute 'eldoc-highlight-function-argument nil ;; hightlight cursor region in echo-area
		      :foreground "yellow"
		      :inherit 'bold)
  (set-face-attribute 'font-lock-string-face nil
		      :foreground "orange"
		      :inherit 'light)
  (set-background-color "black") ;; disable when in terminal for background image
  (set-foreground-color "dark violet") ;; dark violet
  (set-face-background 'isearch-fail "seashell1")
  (set-cursor-color "light green")
  (set-face-background 'region "gray25") ;; The Basic face for highlighting the region / selection/ copy
  (set-frame-parameter nil 'internal-border-width 20) ;; set up / down margin window
  ;; (wallpaper-set "/usr/share/backgrounds/desktop-background-girl.darked.png") ;; run 1 time only needed
  (setq-default left-margin-width 5
		right-margin-width 5
		right-fringe-width 0
		left-fringe-width 0
		line-prefix "      "
		word-wrap t))

(my-backgrounds.theme.el)

;; emacs Transparency. This does not work in terminal. stuff
;; https://www.emacswiki.org/emacs/TransparentEmacs
;; (set-frame-parameter nil 'alpha-background 100)

;; this seems to be for frame and scrolling-bar
;; (add-to-list 'default-frame-alist '(alpha-background . 50))

;; emacs Transparency stuff ends here
;;
;; my-backgrounds.theme thingy ends here
;; -----------------------------------------------------------------------------------------------------

;; -----------------------------------------------------------------------------------------------------
;; mode-line stuff

;; minibar-module-cpu stuff
;; https://codeberg.org/akib/emacs-minibar

(defcustom minibar-module-cpu-cache-for 10
  "Cache CPU load values for this many seconds.  Set to zero disable."
  :type 'number
  :group 'faces)

(defvar minibar--module-cpu-cache nil
  "Cached CPU load.

The value is a cons cell whose car is the CPU load and cdr is the time
when it was recorded.")

(defvar minibar--module-cpu-last-times nil
  "Last CPU idle and total calculated.")

(defvar minibar--module-cpu-count nil
  "Count of processor or CPU.")

(defun minibar--module-cpu-calculate-load (cpu)
  "Calculate CPU load."
  (let* ((last-cell
          (let ((cell (assoc-string
                       cpu minibar--module-cpu-last-times)))
            (unless cell
              (setq minibar--module-cpu-last-times
                    (cons (cons cpu nil)
                          minibar--module-cpu-last-times))
              (setq cell (assoc-string
                          cpu minibar--module-cpu-last-times)))
            cell))
         (last (cdr last-cell))
         (now
          (let ((total 0)
                (active 0)
                (data nil))
            (goto-char (point-min))
            (search-forward cpu)
            (setq data (mapcar (lambda (_) (read (current-buffer)))
                               (number-sequence 0 7)))
            (setq total (apply #'+ data))
            (setq active (- total (+ (nth 3 data)
                                     (nth 4 data))))
            (cons total active))))
    (setcdr last-cell now)
    (* 100
       (if last
           (let ((diff-total (- (car now) (car last)))
                 (diff-active (- (cdr now) (cdr last))))
             (if (zerop diff-total)
                 0.0
               (/ (float diff-active) diff-total)))
         0.0))))

(defun minibar-module-cpu ()
  "Module for showing CPU load."
  (when (or (not minibar--module-cpu-cache)
            (>= (float-time
                 (time-since (cdr minibar--module-cpu-cache)))
                minibar-module-cpu-cache-for))
    (setq
     minibar--module-cpu-cache
     (cons
      (with-temp-buffer
        (insert-file-contents "/proc/stat")
        (unless minibar--module-cpu-count
          (setq minibar--module-cpu-count
                (string-to-number (shell-command-to-string "nproc"))))
        (format
         "%1i%%%s"
         (minibar--module-cpu-calculate-load "cpu")
         (propertize (string) 'face 'bold)
         " "))
      (current-time))))
  (car minibar--module-cpu-cache))

;; minibar-module-cpu stuff ends here

;; Display MEM/WiFi usage in the mode-line stuff
;; https://stackoverflow.com/questions/18336758/how-to-display-uptime-on-emacs-status-bar-i-am-looking-for-usr-bin-uptime-outp

(defun mem-usage ()
  "Display MEM Usage."
  (shell-command-to-string "/home/danrobi/.local/bin/mem_info.sh"))

(defun wifi-usage ()
  "Display WiFi Usage."
  (shell-command-to-string "/home/danrobi/.local/bin/wifi_info.sh"))

;; Display MEM/WIFI usage in the mode-line stuff ends here

;; align-to right stuff
;; https://emacs.stackexchange.com/questions/5529/how-to-right-align-some-items-in-the-modeline

(defun mode-line-fill (reserve)
  "Return empty space using FACE and leaving RESERVE space on the right."
  (when
      (and window-system (eq 'right (get-buffer-window))) ;; get-scroll-bar-mode
    (setq reserve (- reserve 20))) ;; 3
  (propertize " "
	      'display
	      `((space :align-to (- (+ right right-fringe right-margin) ,reserve)))))

;; align-to right stuff ends here

;; update-mode-line stuff

(defun update-mode-line ()
  "Function to update the mode-line."
  (interactive)
  (setq-default mode-line-format
		(list
		 " "
		 mode-line-modified
		 " "
		 (propertize " %b  " 'face '(:foreground "green" :weight bold))
		 "%o "
		 mode-line-modes
		 ;; (propertize "Elfeed: " 'face 'bold)
		 ;; '(:eval (propertize   (elfeed-search--header) 'face 'bold)) ;; elfeed-search--header function in elfeed-search.el
		 ;; Anything above (mode-line-fill) will be align-to-left.
		 ;; Anything below will be aligh-to-right
		 (mode-line-fill 42) ;; position value / right side
		 (propertize "%M" 'face 'bold)
		 ;; display-battery-mode
		 "|"
		 '(:eval (format-time-string "%H:%M %a %b/%d") 'face 'bold) ;; %Y
		 "|"
		 (propertize "C:") ;; 'face 'bold)
		 (minibar-module-cpu)
		 "|"
		 (propertize "M:") ;; 'face 'bold)
		 (mem-usage)
		 "|"
		 (propertize "W:" 'face 'bold)
		 (wifi-usage))))

;; mode-line height
(set-face-attribute 'mode-line nil :height 0.9)

;; Run `update-mode-line` every 10 seconds
(run-with-timer 0 10 #'update-mode-line)

;; update-mode-line stuff ends here
;;

;; (:background "blue-violet" :foreground "snow" :box "0" :weight "ultra-light" :height "0.9"))

(provide 'my-backgrounds)
;;; my-backgrounds.el ends here
