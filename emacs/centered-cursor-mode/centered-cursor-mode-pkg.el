;; -*- no-byte-compile: t; lexical-binding: nil -*-
(define-package "centered-cursor-mode" "20230914.1358"
  "Cursor stays vertically centered."
  ()
  :url "https://github.com/andre-r/centered-cursor-mode.el"
  :commit "67ef719e685407dbc455c7430765e4e685fd95a9"
  :revdesc "67ef719e6854"
  :keywords '("convenience")
  :authors '(("André Riemann" . "andre.riemann@web.de"))
  :maintainers '(("André Riemann" . "andre.riemann@web.de")))
